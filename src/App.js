import React, {Component} from 'react';
import Card from './card/card';
import WinCalculator from './WinCalculator';


class App extends Component {
	
	state = {
		cards: [],
		result: '',
		playerCard: [],
		money: 0
	};
	
	rank = ['a', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k'];
	suit = ['diams', 'spades', 'hearts', 'clubs'];
	
	getCards = () => {
		//let interval = setInterval(() => {
		//Раскомментить чтобы делать поиск комбинаций
			const cards = [];
			const deck = [];
			for (let i = 0; i < this.rank.length; i++) {
				for (let y = 0; y < this.suit.length; y++) {
					let card_x = {
						rank: this.rank[i],
						suit: this.suit[y]
					};
					deck.push(card_x);
				}
			}
			
			while (cards.length < 7) {
				let thisCard = Math.floor(Math.random() * (deck.length));
				let card = deck[thisCard];
				card.id = deck[thisCard].rank + deck[thisCard].suit;
				card.active = '';
				deck.splice(thisCard, 1);
				cards.push(card);
			}
			
			const calc = new WinCalculator(cards, this.suit, this.rank);
			const result = calc.getBestHand();
			const playerCard = cards.splice(cards.length - 2,2);
			let money = this.money;
			switch (result){
				case 'Роял Флеш':
					money = 1000000;
					break;
				case 'Стрит флеш':
					money = 500000;
					break;
				case 'Каре':
					money = 250000;
					break;
				case 'Фулл хаус':
					money = 100000;
					break;
				case 'Флеш':
					money = 50000;
					break;
				case 'Стрит':
					money = 25000;
					break;
				case 'Тройка':
					money = 10000;
					break;
				case 'Две пары':
					money = 5000;
					break;
				case 'Пара':
					money = 1000;
					break;
				default:
					money = 500;
					break;
			}
			this.setState({cards, result, playerCard, money});
			
		// 	if (result === 'Каре')	clearInterval(interval); // Поставляй комбинации, раздает пока не найдет!
		// }, 100);
		//Раскомментить чтобы делать поиск комбинаций
	};
	
	render() {
		let cards = (
			<div className="cardsList">
				{this.state.cards.map(card => {
					const icons = {
						diams: '♦',
						spades: '♠',
						clubs: '♣',
						hearts: '♥'
					};
					return <Card
						key={card.id}
						active={card.active}
						rank={card.rank}
						suit={card.suit}
						suitIcon={icons[card.suit]}
					/>
				})}
			</div>
		);
		
		let playerCards = (
			<div className="cardsList player">
				{this.state.playerCard.map(card => {
					const icons = {
						diams: '♦',
						spades: '♠',
						clubs: '♣',
						hearts: '♥'
					};
					return <Card
						key={card.id}
						active={card.active}
						rank={card.rank}
						suit={card.suit}
						suitIcon={icons[card.suit]}
					/>
				})}
			</div>
		);
		
		return (
			<div className="playingCards faceImages">
				<button onClick={this.getCards} className="button">Раздать карты</button>
				{cards}
				<div className="result">{this.state.result}</div>
				<div className="money">Выигрыш {this.state.money}$</div>
				{playerCards}
			</div>
		);
	}
}

export default App;
